class Reloj {
  var hora = 23
  var minutos = 0
  var segundos = 0

  func setReloj(tiempoEnSegundos: Int){

    //si el tiempo en segundos es menor la cantidad de segundos en una hora, la ora permanece en 0 y operamos solo con los minutos y segundos 
    if tiempoEnSegundos < 3600 {
			hora = 0
			minutos = (tiempoEnSegundos % 3600) / 60
			segundos = tiempoEnSegundos - (minutos * 60)
		} else {
			hora = tiempoEnSegundos / 3600
			minutos = (tiempoEnSegundos % 3600) / 60
			segundos = tiempoEnSegundos - (hora * 3600 + minutos * 60)
		}
		
		if hora > 23 {
			hora = 0
			minutos = 0
			segundos = 0
		}

  }

  //metodos get decada uno de los eleentos del reloj

  func getHora()->Int{
    return hora
  }

  func getMinutos()-> Int{
    return minutos
  }

  func getSegundos()-> Int{
    return segundos
  }
  
  //metodo set de cada elemento del reloj

  func setHora(hora: Int)-> Int{
    self.hora = hora
    return hora
  }

  func setMinutos(minutos: Int)-> Int{
    self.minutos = minutos
    return minutos
  }

  func setSegundos(segundos: Int)-> Int{
    self.segundos = segundos
    return segundos
  }
//metodo tick 
  
  func tick()-> Int{
    self.setReloj(tiempoEnSegundos: segundos)
    let masUnSegundo = segundos + 1
    return masUnSegundo
  }

  // metodo toString() 

  func toString()-> [String]{
    var hour = String(self.getHora())
    var minute = String(self.getMinutos())
    var seconds = String(self.getSegundos())
    var clock = [String]()

    clock.append(hour)
    clock.append(minute)
    clock.append(seconds)
    return clock
  }

  // metodo tickDecrement
  
  func tickDecrement()-> Int{
    self.setReloj(tiempoEnSegundos: segundos)
    let masUnSegundo = segundos - 1
    return masUnSegundo
  }
  
}

var clock = Reloj()
clock.setReloj(tiempoEnSegundos: 3602)
print("La hora es: \(clock.toString())")
print("Segundos menos uno: \(clock.tickDecrement())")
print("Segundos mas uno: \(clock.tick())")